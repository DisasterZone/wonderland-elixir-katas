# AlphabetCipher

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `alphabet_cipher` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:alphabet_cipher, "~> 0.1.0"}]
    end
    ```

  2. Ensure `alphabet_cipher` is started before your application:

    ```elixir
    def application do
      [applications: [:alphabet_cipher]]
    end
    ```

