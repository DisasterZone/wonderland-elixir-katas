# MagicSquare

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `magic_square` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:magic_square, "~> 0.1.0"}]
    end
    ```

  2. Ensure `magic_square` is started before your application:

    ```elixir
    def application do
      [applications: [:magic_square]]
    end
    ```

