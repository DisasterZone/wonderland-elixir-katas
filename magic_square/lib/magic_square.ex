defmodule MagicSquare do

  import Enum, only: [ at: 2, chunk: 2, count: 1, dedup: 1, drop: 2, filter: 2, map: 2, reduce: 3, take_every: 2 ]

  ## Borrowed the list permutations code from 'holsee' on stackoverflow (http://stackoverflow.com/a/36580916) 
  def perms( %MapSet{} = set ) do
    MapSet.to_list(set) |> perms
  end

  def perms( [] ), do: [[]]

  def perms( list ) do
    for h <- list, t <- perms(list -- [h]), do: [h|t]
  end

  def sum_list( list ) do
    reduce( list, 0, &(&1 + &2) )
  end

  def take_every( list, nth, start ) do
    list
    |> drop( start )
    |> take_every( nth )
  end

  def form_rows( list ) do
    chunk( list, 3 )
  end

  def form_cols( list ) do
    ## Which method is more idomatic for elixir, if either? :)

    ##for x <- [ 0, 1, 2 ] do
    ##  take_every( list, 3, x )
    ##end

    [0, 1, 2]
    |> Enum.map( fn elem -> take_every( list, 3, elem ) end)
  end

  def form_diagonals( list ) do
    [
      [at( list, 0 ), at( list, 4 ), at( list, 8 )],
      [at( list, 2 ), at( list, 4 ), at( list, 6 )]
    ]
  end

  def is_valid( list ) do
    rows = form_rows( list )
    cols = form_cols( list )
    diagonals = form_diagonals( list )

    uniq =
      rows ++ cols ++ diagonals
      |> map( fn(elem) -> sum_list( elem ) end )
      |> dedup
      |> count

      { uniq == 1, list }
  end

  def magic_square( ) do
    ## The magic box values
    values = [1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0]
    allPermutations = perms( values )

    allPermutations
    |> map( fn( elem ) -> is_valid( elem ) end )
    |> filter( fn( tuple ) -> elem( tuple, 0 ) == :true end )
  end

end

## Results
##
## [true: [1.5, 4.0, 3.5, 5.0, 3.0, 1.0, 2.5, 2.0, 4.5],
## true: [1.5, 5.0, 2.5, 4.0, 3.0, 2.0, 3.5, 1.0, 4.5],
## true: [2.5, 2.0, 4.5, 5.0, 3.0, 1.0, 1.5, 4.0, 3.5],
## true: [2.5, 5.0, 1.5, 2.0, 3.0, 4.0, 4.5, 1.0, 3.5],
## true: [3.5, 1.0, 4.5, 4.0, 3.0, 2.0, 1.5, 5.0, 2.5],
## true: [3.5, 4.0, 1.5, 1.0, 3.0, 5.0, 4.5, 2.0, 2.5],
## true: [4.5, 1.0, 3.5, 2.0, 3.0, 4.0, 2.5, 5.0, 1.5],
## true: [4.5, 2.0, 2.5, 1.0, 3.0, 5.0, 3.5, 4.0, 1.5]]
