defmodule MagicSquareTest do
  use ExUnit.Case
  doctest MagicSquare

  import MagicSquare

  ## 1.0, 1.5, 2.0
  ## 2.5, 3.0, 3.5
  ## 4.0, 4.5, 5.0

  test "create rows from set" do
    assert [[1.0, 1.5, 2.0], [2.5, 3.0, 3.5], [4.0, 4.5, 5.0]] == form_rows( [1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0] )
  end

  test "create cols from set" do
    assert [[1.0, 2.5, 4.0], [1.5, 3.0, 4.5], [2.0, 3.5, 5.0]] == form_cols( [1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0] )
  end

  test "create diagonals from set" do
    assert [[1.0, 3.0, 5.0], [2.0, 3.0, 4.0]] == form_diagonals( [1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0] )
  end

  test "find perms from set" do
    assert [[1, 2], [2, 1]] == perms(MapSet.new([1,2]))
    assert [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]] == perms(MapSet.new([1,2,3]))
  end

  test "find perms" do
    assert [[1, 2], [2, 1]] == perms([1,2])
    assert [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]] == perms([1,2,3])
  end
end
