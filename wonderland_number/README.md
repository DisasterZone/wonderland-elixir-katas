# WonderlandNumber

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `wonderland_number` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:wonderland_number, "~> 0.1.0"}]
    end
    ```

  2. Ensure `wonderland_number` is started before your application:

    ```elixir
    def application do
      [applications: [:wonderland_number]]
    end
    ```

