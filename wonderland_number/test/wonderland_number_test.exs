defmodule WonderlandNumberTest do
  use ExUnit.Case
  doctest WonderlandNumber

  import WonderlandNumber

  test "check same digit check" do
    assert same_digits( 123456, 654321 ) == true
    assert same_digits( 123456, 563412 ) == true
  end

  test "check when digits are different" do
    assert same_digits( 123456, 554321 ) == false
    assert same_digits( 123456, 563417 ) == false
  end

end
