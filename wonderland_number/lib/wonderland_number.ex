defmodule WonderlandNumber do

  import Enum, only: [ filter: 2 ]

  def same_digits( number1, number2 ) do
    digits1 = Integer.digits( number1 )
    digits2 = Integer.digits( number2 )

    (digits1 -- digits2 == [])
  end

  def wonderland_number( ) do
    100000..999999
    |> filter( fn( elem ) -> same_digits( elem, elem * 2 ) == :true end )
    |> filter( fn( elem ) -> same_digits( elem, elem * 3 ) == :true end )
    |> filter( fn( elem ) -> same_digits( elem, elem * 4 ) == :true end )
    |> filter( fn( elem ) -> same_digits( elem, elem * 5 ) == :true end )
    |> filter( fn( elem ) -> same_digits( elem, elem * 6 ) == :true end )
  end

end

## Results
##
## 142857
##