defmodule DoubletsTest do
  use ExUnit.Case
  doctest Doublets

  import Doublets

  ## Changed tests from the original kata, due to use of much larger word list
  ## This algorithm finds one of the shortest possible answers, but actually solves
  ## for all the possible answers first, so which shortest path is data order dependent.
  test "with word links found" do
    assert doublets( "head",  "tail",  6 ) == [answer: ["head", "heal", "heil", "hail", "tail"]] 
    assert doublets( "door",  "lock",  5 ) == [answer: [ "door",  "dorr",  "dork",  "dock",  "lock" ]]
    assert doublets( "bank",  "loan",  6 ) == [answer: [ "bank",  "bonk",  "book",  "look",  "loon",  "loan" ]]
    assert doublets( "wheat", "bread", 7 ) == [answer: [ "wheat", "cheat", "cleat", "bleat", "bleak", "break", "bread" ]]
  end
end
