defmodule DoubletsUtilsTest do
  use ExUnit.Case
  doctest Doublets.Utils

  import Doublets.Utils

  test "check for neighbors" do
    wordList = [ "cord", "wold", "bold", "sold", "boor" ]

    assert neighbors( wordList, "cold" ) == [ "cord", "wold", "bold", "sold" ]

    wordList2 = [ "cold", "clad", "calo" ]
    assert neighbors( wordList2, "cold" ) == []
  end

  test "check for neighbors, but exclude words that have already been used" do
    wordList = [ "cord", "wold", "bold", "sold", "boor" ]

    assert neighbors_no_history(wordList, "cold", [ "sold" ]) == [ "cord", "wold", "bold" ]
  end

  test "results in accurate count based on the words" do
    assert letters_diff( "ant",  "ant" ) == 0
    assert letters_diff( "ant",  "art" ) == 1
    assert letters_diff( "ant",  "aim" ) == 2
    assert letters_diff( "cart", "tart" ) == 1
    assert letters_diff( "cart", "taut" ) == 2
  end
end