defmodule Doublets.Utils do

  @doc """
  Read in a list of words and trim off the non-printables, primarily newlines
  """
  def read(filename) do
    file = File.open!(filename, [:read])

    IO.stream(file, :line)
    |> Enum.map( &(String.strip(&1)) )
	|> Enum.to_list
  end

  @doc """
  Filter a list to match the size of sample word.
  """
  def size_match(words, word) do
    words
    |> Enum.filter( &( String.length(&1) ) == String.length(word) )
  end

  @doc """
  Requires the words to be matching lengths, should put in a guard.
  """
  def letters_diff(word1, word2) do
    to_char_list(word1)
    |> Enum.zip( to_char_list(word2) )
    |> Enum.filter( &( elem(&1, 0) != elem(&1, 1) ))
    |> Enum.count
  end

  @doc """
  Return neighbor words that are the same length and only one character different
  """
  def neighbors(wordList, word) do
    wordList
    |> Enum.filter( &( letters_diff( &1, word) ) == 1)
  end  

  @doc """
  Return neighbor words that are the same length and only one character different
  but also aren't part of the tree path.
  """
  def neighbors_no_history(wordList, word, history) do
    neighbors = neighbors(wordList, word)
    neighbors -- history
  end

end