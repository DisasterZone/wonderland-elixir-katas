defmodule Doublets do

  import Doublets.Utils
  import Enum, only: [ count: 1, dedup: 1, filter: 2, map: 2, sort: 2, take: 2 ]
  import List, only: [ flatten: 1 ]

  @doc """
  Starter method, that triggers the recursion and interprets the results
  """
  def doublets(word1, word2, threshold \\ 8) do
    words = read("enable1.txt")
    wordList = size_match(words, word1)
    path = []

    result = doublets(wordList, word1, word2, word1, threshold, path, threshold )

    ##IO.write( "Done climbing tree: " )

    result
    |> flatten
    |> filter( fn(x) -> x != nil end )
    |> sort( &(count(elem( &1, 1 )) < count(elem( &2, 1 ))) )
    |> dedup
    |> take(1)
  end
  
  ## This doublets function should only trigger when a valid path is found.
  defp doublets( _wordList, _word1, word2, currentWord, _threshold, path, _solutionDistance )
  when word2 == currentWord
  do
    ## IO.write( "A" )
    { :answer, path ++ [word2] }
  end

  ## Breakout method that triggers when you've reached the threshold without a valid
  ## path having been found.  This threshold is a performance optimization and configurable
  defp doublets( _wordList, _word1, _word2, _currentWord, threshold, _path, solutionDistance )
  when solutionDistance > threshold
  do
    ## Passed the threshold don't recurse
  end
  
  ## Takes a word, determines neighbors and builds a tree while checking for valid path,
  ## the workhorse of this solution.
  defp doublets( wordList, word1, word2, currentWord, threshold, path, _solutionDistance ) do
    neighbors = neighbors_no_history( wordList, currentWord, path )
	
    newPath = path ++ [currentWord]

    ## IO.write( "." )

    pathSize = count( newPath )
    wordsDistance = letters_diff( currentWord, word2 )
    newSolutionDistance = pathSize + wordsDistance

    neighbors
    |> map( &(doublets(wordList, word1, word2, &1, threshold, newPath, newSolutionDistance )) )
  end

end
