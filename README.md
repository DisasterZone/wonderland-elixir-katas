# wonderland-elixir-katas

These are a collection of Elixir
[katas](http://en.wikipedia.org/wiki/Kata_%28programming%29) inspired by
[Lewis Carroll](http://en.wikipedia.org/wiki/Lewis_Carroll) and _Alice
in Wonderland_.

This is a pass at elixir implementations of
[gigasquid](https://github.com/gigasquid/wonderland-clojure-katas) katas for clojure.

![Alice and the tiny door](/images/alicedoor.gif)

>"Curiouser and curiouser!"
>-- - Lewis Carroll, Alice in Wonderland

## How to view the Katas

If you don't have Elixir, follow the instruction at [Install Elixir](http://elixir-land.org/install.html)

Pick a katas of interest _magic_square_.

1. Clone or fork this repo
2. cd 'magic_square'
3. run 'mix test'
4. or 'iex -S mix' followed by ie(1)>'MagicSquare.magic_square'
5. Take a look at the code and feel free to offer constructive feedback.

## License

Copyright 2016 Chris Luff

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.  ( This is to match the gigasquid license )